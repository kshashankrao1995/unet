import torch.nn.functional as F
import torch.nn as nn
import torch

class UNet(nn.Module):
    '''! Class to define the UNet model'''

    def __init__(self):
        super(UNet, self).__init__()
        self.num_classes = 3

    def conv_block(self, input_tensor, in_channel, num_filters, kernel_size = 3):
        """! Create the convolution block for encoder"""
        # Output dim of conv layer = ((I - K + 2P) / S) + 1 -> To keep the output dim same as input, add padding = 1
        x = nn.Conv2d(in_channel, num_filters, kernel_size, padding=1)(input_tensor)
        x = nn.ReLU()(x)
        x = nn.Conv2d(num_filters, num_filters, kernel_size, padding=1)(x)
        x = nn.ReLU()(x)

        return x
    
    def encoder_block(self, input_tensor, in_channel, num_filters, pool_size, dropout):
        """! Conv block + Max pooling """
        conv_out = self.conv_block(input_tensor, in_channel, num_filters, 3)
        pool_out = nn.MaxPool2d(pool_size)(conv_out)
        out = nn.Dropout(dropout)(pool_out)
        return out, conv_out
        
    def encoder(self, input_tensor):
        """! Encoder """
        dropout = 0.3
        p1, c1 = self.encoder_block(input_tensor, 3, 64, 2, dropout)
        p2, c2 = self.encoder_block(p1, 64, 128, 2, dropout)
        p3, c3 = self.encoder_block(p2, 128, 256, 2, dropout)
        p4, c4 = self.encoder_block(p3, 256, 512, 2, dropout)
        return p4, (c1, c2, c3, c4)

    def decoder_block(self, input_tensor, conv_output, in_channel, num_filters, kernel_size, dropout):
        """! Decoder block of UNet"""
        stride = 2
        upsample = nn.ConvTranspose2d(in_channel, num_filters, kernel_size, stride)(input_tensor)   
        cat_out = torch.cat((upsample, conv_output), 1)
        x = nn.Dropout(0.3)(cat_out)
        x = self.conv_block(x, in_channel, num_filters)
        return x
    
    def decoder(self, input_tensor, convs, num_filters):
        """! Decoder"""
        c1, c2, c3, c4 = convs
        kernel_size = 2
        up_sample1 = self.decoder_block(input_tensor, c4, 1024, 512, kernel_size, 0.3)
        up_sample2 = self.decoder_block(up_sample1, c3, 512, 256, kernel_size, 0.3)
        up_sample3 = self.decoder_block(up_sample2, c2, 256, 128, kernel_size, 0.3)
        up_sample4 = self.decoder_block(up_sample3, c1, 128, 64, kernel_size, 0.3)
        
        out = nn.Conv2d(64, num_filters, 1)
        out = nn.Softmax(out)
        
        return out

    def forward(self, x):
        # Encoder
        encoder_out, conv_outs = self.encoder(x)
        
        # Bottleneck layer - Extracts the features without changing the dim (no pool layer)
        bottleneck_out = self.conv_block(encoder_out, 512, 1024, 3)
    
        # Decoder
        decoder_out = self.decoder(bottleneck_out, conv_outs, self.num_classes)

        return decoder_out