from torch.utils.data import Dataset
import numpy as np
import cv2
import os

class DataLoader(Dataset):
    """! Class to load the data and labels """

    def __init__(self, image_dir, mask_dir, txt_file):
        """! Constructor for data loader"""
        self.image_dir = image_dir
        self.mask_dir = mask_dir
        self.image_list = []
        self.mask_list = []
        self.clean_data(txt_file)

    def clean_data(self, txt_file):
        """! Remove file path if opencv does not read it"""
        f = open(txt_file)
        lines = f.readlines()
        for image_name in lines:
            image_name = image_name.strip()
            image_name = image_name.split(" ")[0]
            mask_name = image_name + ".png"
            mask_path = os.path.join(self.mask_dir, mask_name)
            
            if (os.path.isfile(mask_path)):
                self.mask_list.append(mask_path)
                image_path = os.path.join(self.image_dir, image_name)
                self.image_list.append(image_path)
    
    def preprocess_mask(self, mask):
        """! Convert the mask with 3 labels into a binary mask"""
        mask = mask.astype(np.float32)
        mask[mask == 2.0] = 0.0
        mask[(mask == 1.0) | (mask == 3.0)] = 1.0
        return mask

    def __len__(self):
        """! Returns the total number of images/labels"""
        return len(self.image_list)

    def __getitem__(self,idx):
        """! Returns the image and labels for a given index"""
        image = cv2.imread(self.image_list[idx])
        mask = cv2.imread(self.mask_list[idx])
        #mask = preprocess_mask(mask)
        data = {"image": image, "mask": mask}
        return data
