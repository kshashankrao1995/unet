from torchsummary import summary
import matplotlib.pyplot as plt
import numpy as np
import DataLoader
import model
import torch
import cv2


image_dir = r"D:\DeepLearning\unet\data\images"
mask_dir = r"D:\DeepLearning\unet\data\annotations\trimaps"
train_list = r"D:\DeepLearning\unet\data\annotations\trainval.txt"
TrainData = DataLoader.DataLoader(image_dir, mask_dir, train_list)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
net = model.UNet()
net = net.to(device)

# Create random data to test the model
data = torch.rand((1,3,128,128))
data.cuda()

out = net(data)